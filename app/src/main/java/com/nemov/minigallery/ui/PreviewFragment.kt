package com.nemov.minigallery.ui

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.nemov.minigallery.R
import kotlinx.android.synthetic.main.preview_fragment.*

class PreviewFragment : Fragment() {

    private lateinit var viewModel: PreviewViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.preview_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this).get(PreviewViewModel::class.java)
        viewModel.emptyMiniGallery().observe(this, Observer {
            tvContainer.isVisible = it
        })
        viewModel.miniGallery().observe(this, Observer {
            (rvContainer.adapter as PreviewRecyclerViewAdapter).updateValues(it)
        })
        viewModel.navigationIntent().observe(this, Observer {
            when (it) {
                is SuccessNavigation -> {
                    val args = Bundle().apply {
                        putString(ChannelSwapFragment.IMAGE_PATH, it.path)
                    }
                    val navController = Navigation.findNavController(this.requireView())
                    navController.navigate(R.id.action_previewFragment_to_channelSwapFragment, args)
                    viewModel.onConsumeNavigation()
                }
                FailedNavigation -> {
                    Toast.makeText(context, "File unavailable", Toast.LENGTH_SHORT).show()
                    viewModel.onConsumeNavigation()
                }
                InactiveNavigation -> Unit
            }
        })

        with(rvContainer) {
            layoutManager = GridLayoutManager(
                context,
                if (activity!!.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                    3
                } else {
                    6
                }
            )
            adapter = PreviewRecyclerViewAdapter(viewModel)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bImportFromGallery.setOnClickListener {
            imageFromGallery()
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) = data?.run {
        if (resultCode == Activity.RESULT_OK && requestCode == 0) {
            viewModel.onImageSetUpdated(this)
        }
    } ?: Unit

    private fun imageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        val mimeTypes = arrayOf("image/jpeg", "image/png")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent, 0)
    }
}