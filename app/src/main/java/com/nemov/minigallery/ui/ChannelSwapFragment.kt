package com.nemov.minigallery.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.nemov.minigallery.R
import kotlinx.android.synthetic.main.channel_swap_fragment.*

class ChannelSwapFragment : Fragment() {

    companion object {
        const val IMAGE_PATH = "IMAGE_PATH"
    }

    private lateinit var viewModel: ChannelSwapViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.channel_swap_fragment, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this).get(ChannelSwapViewModel::class.java)
        viewModel.editedImage().observe(this, Observer {
            it?.run {
                Glide.with(this@ChannelSwapFragment)
                    .load(this)
                    .error(R.mipmap.ic_launcher)
                    .into(ivEditedImage)
            }
        })

        arguments?.getString(IMAGE_PATH)?.run {
            viewModel.updateSource(this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            val navController = Navigation.findNavController(view)
            navController.navigate(R.id.action_channelSwapFragment_to_previewFragment)
        }
        bSwapChannels.setOnClickListener {
            viewModel.doChannelSwap()
        }
    }
}