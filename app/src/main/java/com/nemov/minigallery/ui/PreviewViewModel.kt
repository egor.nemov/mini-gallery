package com.nemov.minigallery.ui

import android.app.Application
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import androidx.lifecycle.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileInputStream


interface PreviewController {
    fun emptyMiniGallery(): LiveData<Boolean>
    fun miniGallery(): LiveData<List<String>>
    fun navigationIntent(): LiveData<NavigationStatus>
    fun onImageSetUpdated(data: Intent)
    fun onClickItem(path: String)
    fun onConsumeNavigation()
}

sealed class NavigationStatus
data class SuccessNavigation(val path: String): NavigationStatus()
object FailedNavigation: NavigationStatus()
object InactiveNavigation: NavigationStatus()

class PreviewViewModel(application: Application): AndroidViewModel(application), PreviewController {

    private val context: Context = application.applicationContext

    private var data = MutableLiveData<List<String>>().apply {
        value = getMiniGalleryList()
    }

    private var emptyData = MediatorLiveData<Boolean>().apply {
        data.observeForever {
            value = it.isEmpty()
        }
    }

    private var navigationPath = MutableLiveData<NavigationStatus>().apply {
        value = InactiveNavigation
    }

    override fun emptyMiniGallery() = emptyData

    override fun miniGallery() = data

    override fun navigationIntent() = navigationPath

    private fun getMiniGalleryPath() = "${context.getExternalFilesDir(null)}/mini_gallery"

    override fun onImageSetUpdated(data: Intent) {
        val selectedImage: Uri? = data.data
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

        selectedImage?.run {
            val cursor = context.contentResolver?.query(selectedImage, filePathColumn, null, null, null)
            cursor?.run {
                moveToFirst()
                val columnIndex = getColumnIndex(filePathColumn[0])
                val imagePath = getString(columnIndex)
                close()

                savePath(imagePath, getMiniGalleryPath()) {
                    this@PreviewViewModel.data.postValue(getMiniGalleryList())
                }
            }
        }
    }

    override fun onClickItem(path: String) {
        navigationPath.postValue(
            if (File(path).exists()) {
                SuccessNavigation(path)
            } else {
                FailedNavigation
            }
        )
    }

    override fun onConsumeNavigation() {
        navigationPath.postValue(InactiveNavigation)
    }

    private fun getMiniGalleryList(): List<String> {
        val file = File(getMiniGalleryPath())
        return if (file.exists()) {
            val readResult = FileInputStream(file).bufferedReader().use { it.readText() }
            readResult.split("\n")
                .filterNot { it.isBlank() }
                .toSet()
                .toList()
        } else {
            emptyList()
        }
    }

    private fun savePath(fromPath: String, toPath: String, onDone: () -> Unit) =
        GlobalScope.launch {
            File(toPath).parentFile?.mkdir()
            val file = File(toPath)
            if (file.exists()) {
                file.appendText("\n")
                file.appendText(fromPath)
            } else {
                file.createNewFile()
                file.appendText(fromPath)
            }
            onDone()
        }
}