package com.nemov.minigallery.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nemov.minigallery.R
import kotlinx.android.synthetic.main.fragment_preview_list_item.view.*


class PreviewRecyclerViewAdapter(private val viewModel: PreviewController): RecyclerView.Adapter<PreviewRecyclerViewAdapter.ViewHolder>() {

    private val values = mutableListOf<String>()

    fun updateValues(data: List<String>) {
        values.clear()
        values.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_preview_list_item, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        Glide.with(holder.view.context)
            .load(item)
            .error(R.mipmap.ic_launcher)
            .into(holder.preview)

        holder.view.setOnClickListener {
            viewModel.onClickItem(item)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val preview: ImageView = view.ivMiniPreview
    }
}