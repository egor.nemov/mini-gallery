package com.nemov.minigallery

import android.Manifest
import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

class MainActivity : AppCompatActivity() {

    private val permissions = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!arePermissionsGranted(permissions)) {
            ActivityCompat.requestPermissions(this@MainActivity, permissions,
                    REQUEST_PERMISSION
            )
        }
    }

    private fun arePermissionsGranted(permissions: Array<String>) =
            permissions
                    .map { isPermissionGranted(it) }
                    .reduce { acc, b -> acc && b  }

    @TargetApi(23)
    private fun isPermissionGranted(permission: String) =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
            } else true

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when {
            requestCode == REQUEST_PERMISSION && grantResults.none { it == -1 } -> Unit
            requestCode == REQUEST_PERMISSION -> {
                ActivityCompat.requestPermissions(this, this.permissions,
                        REQUEST_PERMISSION
                )
            }
        }
    }

    companion object {
        private const val REQUEST_PERMISSION = 42
    }
}